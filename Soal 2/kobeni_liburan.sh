#!/bin/bash

#mendapatkan data waktu (jam) sekarang
HOUR=$(date +%H)
# Me-Check apabila waktu jam saat ini adalah 0 maka akan dilakukan dowload gambar sebanyak 1 kali
if [ "$HOUR" -eq 0 ]; then
    jumlah_download=1
else
    # Jika tidak, maka akan dilakukan dowload gambar sebanyak waktu jam saat ini!
    jumlah_download="$HOUR"
fi


if [ "$1" = "download" ]; then 
#memasukkan kedalam folder "kumpulan_(no folder)"
FileDir="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
    mkdir "$FileDir"

# Looping sesuai waktu
for ((p=1; p<=$jumlah_download; p++))
do
  # Download gambar
  FileNama="perjalanan_$p"
  echo "$filename"
  wget http://itspreneur.com/wp-content/uploads/2023/03/$FileNama.jpg -O ./$FileDir/$FileNama.jpg
done

#jika file diinginkan berupa zip
elif [ "$1" = "zip" ]; then

JumlahDevil=$(ls -l | grep -c "devil_[0-9]*\.zip")
NextDevil="devil_$((JumlahDevil + 1)).zip"
DIRECTORIES = $(find . -type d -name "kumpulan_*")
zip -r $NextDevil $DIRECTORIES

else
    echo "Cara pengisian : bash kobeni_liburan.sh [download/zip]"
    exit 1
fi