#!/bin/bash
# register louis.sh

#membuat dir
mkdir -p "users"

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

function main {

  #Registration System
  #Masukan username dan password baru

  read -p "uname: " username

  read -p "pass: " password

  if [[ "$(grep "$uname" "$users_list")" ]]; then
    	echo -e "\nUsername sudah terdaftar"
    	log "ERROR" "User already exists"
    exit 1
  fi

  if [[ "${#password}" -lt 8 ]]; then
    	echo -e "\npass minimal 8 karakter"
    exit 1
  fi

  if [[ ! $(echo "$pass" | grep '[a-z]' | grep '[A-Z]') ]]; then
    	echo -e "\pPass harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
  fi

  if [[ "$pass" =~ [^a-zA-Z0-9] ]]; then
    	echo -e "\npass harus menggunakan karakter alphanumeric"
    exit 1
  fi

  if [[ "$pass" == "$uname" ]]; then
    	echo -e "\npass tidak boleh sama dengan username"
    exit 1
  fi

  if [[ $(echo "$pass" | grep -i 'chicken') ]] || [[ $(echo "$pass" | grep -i 'ernie') ]]; then
    	echo -e "\npass tidak dapat menggunakan kata chicken atau ernie"
    exit 1
  fi

  echo "$uname:$pass" >> "$users_list"
  echo -e "\nAkun telah dibuat"
  log "INFO" "User $uname registered"
}

main
