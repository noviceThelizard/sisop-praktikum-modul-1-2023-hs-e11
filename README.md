# sisop-praktikum-modul-1-2023-HS-E11



## Anggota Kelompok E11

Sastiara Maulikh(5025201257)

Muhammad Febriansyah (5025211164)

Schaquille Devlin Aristano (5025211211)

# Soal 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi 


## Soal 1 bagian 1
Tampilkan 5 Universitas dengan ranking tertinggi di Jepang

## Cara Penyelesaian
Digunakan syntac ```grep``` untuk mengambil baris yang terdapat kata 'japan' dan dilanjutkan menggukanan ```sort``` untuk menyortir dari kolom pertama ```-k 1```, lalu untuk mengambil 5 baris teratas dari hasil yang di sort di berikan syntac ``` head -n 5``` dan ```awk -F "," '{print $1","$2","$3}``` guna untuk menampilkan hasil dari kolom pertama, kedua, dan ketiga yang dipisahkan oleh koma (",").

## Source Code
```sh
#!/bin/bash
echo -e "\n\n*** Tampilkan 5 Universitas dengan ranking tertinggi di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | awk -F "," '{print $1","$2","$3}'
```

## Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1A.png)

# Soal 1 bagian 2
Keluarkan output Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang

### Cara Penyelesaian
penggunaan metode hampir sama dengan soal 1 bagian 1 namun karena di sort lagi untuk menemukan nilai frs score yang rendah yang mana nilai frs terdapat pada kolom 9, sehingga ditambahkan```sort -t , -k 9``` dan ```head -n 1``` untuk menampilkan satu univ saja.

### Source Code
```sh
#!/bin/bash
echo -e "\n\n*** mencari Faculty Student Score yang paling rendah diantara 5 Universitas di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | sort -t, -k 9 | head -n 1 | awk -F "," '{print $2","$9}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1B.png)

## soal 1 bagian 3
Cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi

### Cara Penyelesaian
Digunakan syntac ```grep``` untuk mencari variable 'Japan' pada file csv lalu dilakukan ```sort``` dengan ranking Employment Outcome Rank(ger rank) pada kolom ke 20 dan diberi batasan ```head -n 10``` untuk mengambil 10 universitas teratas. lalu dilanjutkan menggunakan awk untuk mengprint kolom 2 dan kolom 20 sebagai output

### Source Code
```sh
#!/bin/bash
echo -e "\n\n*** cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi ***"
grep 'Japan' university_data.csv | sort -n -t , -k 20 | head -n 10 | awk -F "," '{print $2","$20}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1231.png)

## Soal 1 bagian 4
Cari universitas tersebut dengan kata kunci keren

### Cara Penyelesaian
digunakan syntax ```grep``` guna untuk mendapatkan data baris yang memiliki kata "keren" , lalu dilanjutkan menggunakan ```awk``` untuk menampilkan hasil output pada kolom 1,2, dan 3

### Source Kode
```sh
echo -e "\n*** universitas dengan kata kunci 'keren' ***"
grep -i "keren" university_data.csv | awk -F "," '{print $1","$2","$3}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1D.png)

# Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut

### Soal 2 bagian 1
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat(kumpulan_1, kumpulan_2, dst)

### Cara Penyelesaian

buat suatu tambahan untuk mengetahui bahwa itu adalah perintah untuk melakukan dowload gambar berdasarkan waktu sekarang
```sh
bash university_survey.sh download
```
jika inputan sesuai maka program dapat berjalan

mula-mula buat ```$HOUR``` untuk menyimpan jam sekarang, namun lakukan perintah ```if/else``` untuk mengubah jika terjadi kondisi saat jam saat ini = 0, maka harus dihitung 1. Simpan perubahan pada ```$jumlah_download```

```sh


#mendapatkan data waktu (jam) sekarang
HOUR=$(date +%H)
# Me-Check apabila waktu jam saat ini adalah 0 maka akan dilakukan dowload gambar sebanyak 1 kali
if [ "$HOUR" -eq 0 ]; then
    jumlah_download=1
else
    # Jika tidak, maka akan dilakukan dowload gambar sebanyak waktu jam saat ini!
    jumlah_download="$HOUR"
fi
```

lalu buat sebuah file baru dengan menggukanan syntax ```mkdir``` pada $FileDir ,dengan format ```$FileDir = "kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"```

kemudian lakukan looping (n) dengan batasan 1 sampai kurang dari sama dengan waktu saat ini. dan didalam looping dowload gambar dengan syntax ```wget``` lalu diletakkan pada folder yang telah dibuat kumpulan_[no]/perjalanan_$n.png

### Source Code
```sh

if [ "$1" = "download" ]; then 
#memasukkan kedalam folder "kumpulan_(no folder)"
FileDir="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
    mkdir "$FileDir"

# Looping sesuai waktu
for ((p=1; p<=$jumlah_download; p++))
do
  # Download gambar
  FileNama="perjalanan_$p"
  echo "$filename"
  wget http://itspreneur.com/wp-content/uploads/2023/03/$FileNama.jpg -O ./$FileDir/$FileNama.jpg
done
```

## Soal 2 bagian 2

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas

### Cara Penyelesaian

buat suatu tambahan untuk mengetahui bahwa itu adalah perintah untuk melakukan force zip pada suatu folder , sbb :
```sh
bash university_survey.sh zip
```

simpan input pada ```$1```. dan digunakan ```if/else``` untuk menceknya. didalam if/else terdapat ```$JumlahDevil =$(ls -l | grep -c "devil_[0-9]*\.zip")``` guna untuk mencek/menghitung jumlah zip yang sudah terdapat dalam folder. lalu nama folder akan +1 sesuai dengan format penamaan yang sudah ditentukan seperti devil_1 dan seterusnya dimana yang di zip adalah folder kumpulan.

### Source Code
``` sh
#jika file diinginkan berupa zip
elif [ "$1" = "zip" ]; then

JumlahDevil=$(ls -l | grep -c "devil_[0-9]*\.zip")
NextDevil="devil_$((JumlahDevil + 1)).zip"
DIRECTORIES = $(find . -type d -name "kumpulan_*")
zip -r $NextDevil $DIRECTORIES

else
    echo "Cara pengisian : bash kobeni_liburan.sh [download/zip]"
    exit 1
fi
```

### All Source Code no 2
``` sh
#!/bin/bash

#mendapatkan data waktu (jam) sekarang
HOUR=$(date +%H)
# Me-Check apabila waktu jam saat ini adalah 0 maka akan dilakukan dowload gambar sebanyak 1 kali
if [ "$HOUR" -eq 0 ]; then
    jumlah_download=1
else
    # Jika tidak, maka akan dilakukan dowload gambar sebanyak waktu jam saat ini!
    jumlah_download="$HOUR"
fi


if [ "$1" = "download" ]; then 
#memasukkan kedalam folder "kumpulan_(no folder)"
FileDir="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
    mkdir "$FileDir"

# Looping sesuai waktu
for ((p=1; p<=$jumlah_download; p++))
do
  # Download gambar
  FileNama="perjalanan_$p"
  echo "$filename"
  wget http://itspreneur.com/wp-content/uploads/2023/03/$FileNama.jpg -O ./$FileDir/$FileNama.jpg
done

#jika file diinginkan berupa zip
elif [ "$1" = "zip" ]; then

JumlahDevil=$(ls -l | grep -c "devil_[0-9]*\.zip")
NextDevil="devil_$((JumlahDevil + 1)).zip"
DIRECTORIES = $(find . -type d -name "kumpulan_*")
zip -r $NextDevil $DIRECTORIES

else
    echo "Cara pengisian : bash kobeni_liburan.sh [download/zip]"
    exit 1
fi
```



- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
